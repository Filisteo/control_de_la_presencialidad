<?php

//El siguiente controlador solo manejará las páginas estáticas de la aplicación

class Paginas extends Controller
{
    public function __construct()
    {
        
    }

    //Método que carga la vista de paginas index
    public function index(){
        $this->view('paginas/index');
    }
}
?>