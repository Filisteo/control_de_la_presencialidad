<?php
 use \Tamtamchik\SimpleFlash\Flash;

 //El controlador supervisor es un usuario cuyas funciones son registrar, editar, eliminar y asesorar la presencia de sus empleados

class Supervisors extends Controller
{

    private $user;

    public function __construct()
    {
        $user = $this->userModel = $this->model('User');
    }

    //INSERT, añade datos de un nuevo empleado
    public function add()
    {
        //Detecta si recibe un método POST
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            //Filtra el método POST para evitar iyecciones SQL, "Sanitizando" los strings
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            //Almacena los datos recibidos en el formulario en un array tipo clave -> valor
            $data = [
                'DNI' => trim($_POST['DNI']),
                'name' => trim($_POST['name']),
                'image' => !empty($_FILES) ? $_FILES['image']['name'] : '',
                'last-name-1' => trim($_POST['last-name-1']),
                'last-name-2' => trim($_POST['last-name-2']),
                'phone-number' => trim($_POST['phone-number']),
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'DNI_err' => '',
                'name_err' => '',
                'image_err' => '',
                'lastname1_err' => '',
                'lastname2_err' => '',
                'phone-number_err' => '',
                'email_err' => '',
                'password_err' => ''
            ];

            //COMPROBANDO ERRORES

            //DNI error
            //Expresión regular, que contenga de 7 a 8 números y una letra mayúscula al final 
            $patron = '/[0-9]{7,8}[A-Z]/';
            if (empty($data['DNI'])) {
                $data['DNI_err'] = 'Campo vacio';
                //Comprueba si la extensión de la cadena no es de 9
            } elseif (strlen($data['DNI']) != 9) {
                $data['DNI_err'] = 'El DNI debe tener 9 dígitos';
                //Comprueba si la expresión lógica cumple con la expresión regular
            } elseif (preg_match($patron, $data['DNI']) == 0) {
                $data['DNI_err'] = 'Debe tener un caracter en mayúscula y 8 dígitos';
            }

            //Nombre error
            if (empty($data['name'])) {
                $data['name_err'] = 'Campo vacio';
            }

            //Imagen error
            if (!empty($data['image'])) {
                $arrType = ["image/jpeg", "image/png", "image/gif"];
                $file = new File($_FILES['image'], $arrType);
                var_dump($data['image']);
                try {
                    $file->prubeCase();
                    $file->saveUploadFile('img/');
                } catch (FileException $error) {
                    $data['image_err'] = $error->getMessage();
                }
            }

            //Apellido 1
            if (empty($data['last-name-1'])) {
                $data['lastname1_err'] = 'Campo vacio';
            }

            //Apellido 2
            if (empty($data['last-name-2'])) {
                $data['lastname2_err'] = 'Campo vacio';
            }

            //Teléfono            
            if (empty($data['phone-number'])) {
                $data['phone-number_err'] = 'Campo vacio';
                //el método get_númeric revisa si el argumento es un número o no
            } elseif (!get_numeric($data['phone-number'])) {
                $data['phone-number_err'] = 'No es un número de teléfono';
            } elseif (strlen($data['phone-number']) != 9) {
                $data['phone-number_err'] = 'El número de teléfono debe tener 9 dígitos';
            }

            //Email
            if (empty($data['email'])) {
                $data['email_err'] = 'Campo vacio';
            } elseif (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $data['email_err'] = 'Correo no valido';
            }

            //Password
            if (empty($data['password'])) {
                $data['password_err'] = 'Campo vacio';
            } elseif (strlen($data['password']) < 6) {
                $data['password_err'] = 'La contraseña tiene que tener 6 carácteres';
            }

            if (empty($data['DNI_err']) && empty($data['name_err']) && empty($data['lastname1_err']) && empty($data['lastname2_err']) 
            && empty($data['phone-number_err']) && empty($data['email_err']) && empty($data['password_err']) && empty($data['image_err'])) {
                
                $data = [
                    'DNI' => trim($_POST['DNI']),
                    'name' => trim($_POST['name']),
                    'image' => !empty($_FILES) ? $_FILES['image']['name'] : '',
                    'last-name-1' => trim($_POST['last-name-1']),
                    'last-name-2' => trim($_POST['last-name-2']),
                    'phone-number' => get_numeric($data['phone-number']),
                    'email' => trim($_POST['email']),
                    'password' => trim(password_hash($_POST['password'], PASSWORD_DEFAULT)),
                ];

                //Llama al método insert del modelo User, para ello, le paso como argumento el array anterior
                $this->userModel->addWorker($data);
                
                //Instancia objeto, permitiendo enviar un mensaje a la vista
                $flash = new Flash();
                $flash->message('Worker inserted.', 'info');
                //Redirecciona al controlador users y carga el método supervisor, quien redirige a la vista supervisor
                redirect('/users/supervisor');
            } else {
                $this->view('supervisors/add', $data);
            }
        } else {

            $data = [
                'DNI' => '',
                'name' => '',
                'last-name-1' => '',
                'last-name-2' => '',
                'phone-number' => '',
                'email' => '',
                'password' => '',
                'image' => ''
            ];

            $this->view('supervisors/add', $data);
        }
    }
    
    //DELETE
    public function delete($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->userModel->deleteWorker($id);
            $flash = new Flash();
            $flash->message('Worker delete.', 'danger');
            redirect('/users/supervisor');
        }
    }
    //UPDATE

    public function edit($id)
    {

        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            
            $data = [
                'id' => $id,
                'DNI' => trim($_POST['DNI']),
                'name' => trim($_POST['name']),
                'image' => !empty($_FILES) ? $_FILES['image']['name'] : '',
                'last-name-1' => trim($_POST['last-name-1']),
                'last-name-2' => trim($_POST['last-name-2']),
                'phone-number' => trim($_POST['phone-number']),
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'DNI_err' => '',
                'name_err' => '',
                'image_err' => '',
                'lastname1_err' => '',
                'lastname2_err' => '',
                'phone-number_err' => '',
                'email_err' => '',
                'password_err' => ''
            ];

            //COMPROBANDO ERRORES

            //DNI error
            $patron = '/[0-9]{7,8}[A-Z]/';
            if (empty($data['DNI'])) {
                $data['DNI_err'] = 'Campo vacio';
            } elseif (strlen($data['DNI']) != 9) {
                $data['DNI_err'] = 'El DNI debe tener 9 dígitos';
            } elseif (preg_match($patron, $data['DNI']) == 0) {
                $data['DNI_err'] = 'Debe tener un caracter en mayúscula y 8 dígitos';
            }

            //Nombre error
            if(empty($data['name'])) {
                $data['name_err'] = 'Campo vacio';
            }

            //Imagen error
            if (!empty($data['image'])) {
                $arrType = ["image/jpeg", "image/png", "image/gif"];
                $file = new File($_FILES['image'], $arrType);
                var_dump($data['image']);
                try {
                    $file->prubeCase();
                    $file->saveUploadFile('img/');
                } catch (FileException $error) {
                    $data['image_err'] = $error->getMessage();
                }
            }

            //Apellido 1
            if (empty($data['last-name-1'])) {
                $data['lastname1_err'] = 'Campo vacio';
            }

            //Apellido 2
            if (empty($data['last-name-2'])) {
                $data['lastname2_err'] = 'Campo vacio';
            }

            //Teléfono            
            if (empty($data['phone-number'])) {
                $data['phone-number_err'] = 'Campo vacio';
            } elseif (!get_numeric($data['phone-number'])) {
                $data['phone-number_err'] = 'No es un número de teléfono';
            } elseif (strlen($data['phone-number']) != 9) {
                $data['phone-number_err'] = 'El número de teléfono debe tener 9 dígitos';
            }

            //Email
            if (empty($data['email'])) {
                $data['email_err'] = 'Campo vacio';
            } elseif (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $data['email_err'] = 'Correo no valido';
            }

            //Password
            if (empty($data['password'])) {
                $data['password_err'] = 'Campo vacio';
            } elseif (strlen($data['password']) < 6) {
                $data['password_err'] = 'La contraseña tiene que tener 6 carácteres';
            }

            if (empty($data['DNI_err']) && empty($data['name_err']) && empty($data['lastname1_err']) && empty($data['lastname2_err']) && empty($data['phone-number_err']) && empty($data['email_err']) && empty($data['password_err'])) {
               
                $data = [
                    'id' => $id,
                    'DNI' => trim($_POST['DNI']),
                    'name' => trim($_POST['name']),
                    'image' => !empty($_FILES) ? $_FILES['image']['name'] : '',
                    'last-name-1' => trim($_POST['last-name-1']),
                    'last-name-2' => trim($_POST['last-name-2']),
                    'phone-number' => get_numeric($data['phone-number']),
                    'email' => trim($_POST['email']),
                    'password' => trim(password_hash($_POST['password'], PASSWORD_DEFAULT)),
                ];

                $this->userModel->updateWorker($data);
                $flash = new Flash();
                $flash->message('Worker updated.', 'warning');
                redirect('/users/supervisor');
            } else {
                $this->view('supervisors/edit', $data); 
            }

        }else{

            $userEdit = $this->userModel->getUser($id);

            $data = [
                'user' => $userEdit
            ];
    
            $this->view('supervisors/edit', $data);
        }
    }
}
