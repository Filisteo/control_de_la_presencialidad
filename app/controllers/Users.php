<?php
    use \Tamtamchik\SimpleFlash\Flash;
    class Users extends Controller{

        public function __construct() {
            $this->userModel = $this->model('User');
        }

        public function login(){

            //El helper isLoggedh verifica si el usuario posee variables de sesión, dependiendo de esto, se redirige a la vista worker o supervisor
            if(isLoggedh());

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $_POST = filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);

                $data = [
                    'email' =>  trim($_POST['email']),
                    'password' =>  trim($_POST['password']),
                    'email_err' => '',
                    'password_err' => '',
                ];

                
                /////////////////////////////// PASSWORD /////////////////////////////////////////////
                if(empty($data['password'])){
                    $data['password_err'] = 'Campo vacio';
                }elseif(strlen($data['password']) < 6){                                    
                    $data['password_err'] = 'La contraseña tiene que tener 6 carácteres';
                }

                /////////////////////////////// EMAIL ///////////////////////////////////////////////

                if(empty($data['email'])){
                    $data['email_err'] = 'Campo vacio';
                }

                /////////////////////////////// ERRORES ///////////////////////////////////////////////
                
                if(empty($data['email_err']) && empty($data['password_err'])){
                    //Comprueba en la base de datos si existe el usuario y la contraseña es correcta, devolviendo un objeto si es cierto
                    $user = $this->userModel->login($data['email'], $data['password']);

                    if($user){
                        //Crea las variables de sesión para el usuario
                        $this->createUserSession($user);
                    }else{
                        $data['password_err'] = 'Contraseña incorrecta';
                        $this->view('users/login', $data);
                    }

                }else{
                    $this->view('users/login', $data);
                }

                $this->view('users/login', $data);
            
            }else{

                $data = [
                    'email' => '',
                    'password' => '',
                    'email_err' => '',
                    'password_err' => '',
                    'activa' => 'login'
                ];

                $this->view('users/login', $data);
            }
        }

        //Crea variables de sesión para el usuario.
        public function createUserSession($user){
            $flash = new Flash();
            $_SESSION['id'] = $user->id ;
            $_SESSION['name'] = $user->Nombre;
            $_SESSION['email'] = $user->Email;
            $_SESSION['workstation'] = $user->Area_de_Trabajo_idArea;
            $_SESSION['ids'] = $user->Supervisor_id; 
            
            //SUPERVISOR, de serlo, redirecionalo a la vista supervisor, por el controlador y el método pasados como argumento
            if($user->Supervisor_id != null){
                $flash->message('Welcome supervisor.', 'info');
                redirect('/users/supervisor');
            }
            $flash->message('Good morging partner.', 'info');
            redirect('/users/worker');
        }

        public function worker(){
            //Obtiene los datos del trabajador y su puesto de trabajo correspondiente
            //TRABAJO
            $work = $this->userModel->workstation($_SESSION['workstation']);
            //TRABAJADOR
            $worker = $this->userModel->getUser($_SESSION['id']);
            
            $data = [
                'work' =>  $work,
                'worker' => $worker
            ];

            $this->view('users/worker', $data);
            //CONDICION PARA CAPTURAR EL LLAMADO DE UN BOTON Y CREA UN TURNO
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
               
                $fecha = date("Y-m-d H:i:s");

                if(!isset($_SESSION['boton'])){
                    $this->userModel->inicioTurno($_SESSION['id'], $fecha);
                }elseif(isset($_SESSION['inicio'])){
                    $this->userModel->turno();
                    $this->userModel->finTurno($fecha);
                    redirect('/users/logout');
                }    
                
            }
            
        }


        //LOGOUT
        public function logout(){
            //Vacia las variables de sesión y posteriormente, destruyela
            session_unset();
            session_destroy();
            redirect('/paginas/index');
        }


        //ME MANDA A LA VISTA DEL SUPERVISOR
        public function supervisor(){
            //Obtengo la estación de trabajo del supervisor, los trabajadores de esa estación, y de la existencia de su presencialidad
            $work = $this->userModel->workstation($_SESSION['workstation']);
            $worker = $this->userModel->getUsers($_SESSION['workstation']);
            $presence = $this->userModel->presence($_SESSION['workstation']);

            $data = [
                'work' =>  $work,
                'workers'=> $worker,
                'presence' => $presence
            ];

            $this->view('supervisors/supervisor',$data);
        }
    }

?>