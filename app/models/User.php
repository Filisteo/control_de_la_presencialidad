<?php 

    class User {
        private $db;

        public function __construct()
        {
            $this->db = new Database();
        }
        //LOGIN, revisa si existe el usuario, si la contraseña es correcta, de serlo, envia los datos del usuario como un objeto 
        public function login($email, $password){

            $this->db->query("SELECT * FROM Trabajadores WHERE Email =  :email");

            $this->db->bind(':email', $email);

            $result = $this->db->single('User');

            if(password_verify($password, $result->Password) || $result->Password == $password){
                return $result;
            }else{
                return false;
            }      
        }

        // Pulsado el botón, carga este método, el cual registra la id del trabajador, y la fecha-hora actual
        public function inicioTurno($id, $time){
            $this->db->query('INSERT INTO Turno (inicio, Trabajadores_id) VALUES (:inicio, :id)');
    
            $this->db->bind(':inicio', $time);
            $this->db->bind(':id', $id);
            $this->db->execute();
            $_SESSION['inicio'] = $time;
        }

        //Creado el turno, selecciona el mismo turno cuya fecha del campo inicio, es el de la variable de la sesión.
        public function turno(){
            $this->db->query("SELECT * FROM Turno WHERE inicio =  :inicio"); 
            $this->db->bind(':inicio',  $_SESSION['inicio']);  
            //Devolvera un objeto
            $result = $this->db->single('User');

            $_SESSION['turno'] = $result->idTurno;
            $_SESSION['inicio'] = $result->inicio;

        }

        //El objeto sera actualizado con una fecha de finalización del turno, mediante un argumento que es la fecha
        //para ello útilizo la variable de sesión turno, la cual obtengo del método anterior.
        public function finTurno($time){
            $this->db->query('UPDATE Turno SET final = :final WHERE idTurno = :idTurno');
            $this->db->bind(':idTurno', $_SESSION['turno']);
            $this->db->bind(':final', $time);
            $this->db->execute();
        }

        //Obtener los datos del puesto de trabajo al que pertenece esa id
        public function workstation($id){
            $this->db->query("SELECT Nombre_Area FROM Area_de_Trabajo WHERE idArea =  :id");
            $this->db->bind(':id',  $id);
            $result = $this->db->single('User');
            return $result;
        }

        //Devuelve todos los usuarios cuyo area de trabajo es el especificado por la id y no es supervisor
        public function getUsers($id){
            $this->db->query("SELECT * FROM Trabajadores WHERE Area_de_Trabajo_idArea = :id AND Supervisor_id  IS NULL");
            $this->db->bind(':id',  $id);
            $results = $this->db->resultSet('User');
            return $results;
        }

        //INSERT
        public function addWorker($data){

            $this->db->query("INSERT INTO Trabajadores (DNI, Nombre, image, Apellido_1, Apellido_2, Teléfono, Email, Password, Area_de_Trabajo_idArea) 
            VALUES (:DNI, :Nombre, :image, :Apellido1, :Apellido2, :Telefono, :Email, :Password, :AreaT)");

            $this->db->bind(':DNI',  $data['DNI']);
            $this->db->bind(':Nombre',  $data['name']);
            $this->db->bind(':image',  $data['image']);
            $this->db->bind(':Apellido1',  $data['last-name-1']);
            $this->db->bind(':Apellido2',  $data['last-name-2']);
            $this->db->bind(':Telefono',   $data['phone-number']);
            $this->db->bind(':Email',  $data['email']);
            $this->db->bind(':Password',  $data['password']);
            $this->db->bind(':AreaT',  $_SESSION['workstation']);

            $this->db->execute();
        }

        //DELETE
        public function getUser($id){
            $this->db->query("SELECT * FROM Trabajadores WHERE id = :id");
            $this->db->bind(':id',  $id);
            $results = $this->db->single('User');
            return $results;
        }

        public function deleteWorker($id){
            $this->db->query("DELETE FROM Trabajadores WHERE id = :id");
            $this->db->bind(':id', $id);
            $this->db->execute();
        }

        //UPDATE
        public function updateWorker($data){
            $this->db->query("UPDATE Trabajadores SET 
            DNI = :DNI, Nombre = :Nombre, image = :image, Apellido_1 = :Apellido1, Apellido_2 = :Apellido2, Teléfono = :Telefono, Email = :Email, 
            Password = :Password, Area_de_Trabajo_idArea = :AreaT WHERE id = :id");

            $this->db->bind(':id', $data['id']);
            $this->db->bind(':DNI',  $data['DNI']);
            $this->db->bind(':Nombre',  $data['name']);
            $this->db->bind(':image',  $data['image']);
            $this->db->bind(':Apellido1',  $data['last-name-1']);
            $this->db->bind(':Apellido2',  $data['last-name-2']);
            $this->db->bind(':Telefono',  $data['phone-number']);
            $this->db->bind(':Email',  $data['email']);
            $this->db->bind(':Password',  $data['password']);
            $this->db->bind(':AreaT',  $_SESSION['workstation']);

            $this->db->execute();
        }

        //El método devuelve un array de objetos con los turnos de los trabajadores, en este caso
        //tiene como condición devolver los objetos cuyo día sea el actual y cuyo turno tanto
        //de inicio como de fin sumen 8 horas
        public function presence($id){
            $this->db->query("SELECT * FROM Trabajadores
            LEFT JOIN Turno ON Trabajadores_id = id AND DAY(inicio) = DAY(CURDATE()) AND HOUR(final) >= HOUR(inicio)+8
            WHERE Area_de_Trabajo_idArea = :idArea AND Supervisor_id IS NULL");  
            $this->db->bind(':idArea', $id);
            $results = $this->db->resultSet('User');
            return $results;
        }

        public function time($id){
            $this->db->query("SELECT * FROM Turno");
        }
    }

?>