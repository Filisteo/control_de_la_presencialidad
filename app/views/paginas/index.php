<?php 
    include_once APPROOT.'/views/partials/header.php';
    include_once APPROOT.'/views/partials/navbar.php';
?>
<div class="flashes">
    <?= (string) flash() ?>
</div>
    <h1 id="h1" class="text-primary">
        Welcome to your work place
    </h1>
    <p class="text-primary">
        This is a way to ensure your presence in the workplace, 
        in this way we can also record your working hours performed 
        to make you aware of the time you spend in your work.
    </p>
    <a id="a-body-1" class="btn btn-primary" href="<?= URLROOT?>/users/login" ><i class="fas fa-sign-in-alt"></i>Login</a>
<?php 
    include_once APPROOT.'/views/partials/footer.php';
?>