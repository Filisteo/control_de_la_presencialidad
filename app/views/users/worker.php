<?php
include_once APPROOT . '/views/partials/header.php';
include_once APPROOT . '/views/partials/navbar.php';

?>

<div class="flashes">
    <?= (string) flash() ?>
</div>
<div id="card-worker" class="card text-primary">
    <img id="img" src="<?= URLROOT.'/public/img/'.$data['worker']->image ?>" class="card-img-top rounded-1 rounded-circle" alt="...">
    <div class="card-body">
        <h5 class="card-title text-center mb-4">Wellkome to your workplace</h5>
        <ul>
            <li>Name:
                <?= $_SESSION['name'] ?>
            </li>
            <li>Workstation: <?= $data['work']->Nombre_Area ?></li>
        </ul>
        <?php
        //Dependiendo de si se ha pulsado el botón o no, cambia el texto y la forma del botón.
        if (!isset($_SESSION['boton'])) {
        ?>
            <h2 class="text-center">Start work shift</h2>
        <?php
        } else {
        ?>
            <h2 class="text-center">End work shift</h2>

        <?php } ?>

        <form method="post">
            <?php
            if (!isset($_SESSION['boton'])) {
                $_SESSION['boton'] = true;
            ?>
                <button type="submit" id="a-working" class="btn btn-success"><i id="check" class="fas fa-check"></i></button>

            <?php
            } else {
                unset($_SESSION['boton'])
            ?>
                <button type="submit" id="a-working" class="btn btn-warning"><i id="check" class="fas fa-house-user"></i></button>
            <?php
            }
            ?>
        </form>
    </div>
</div>
<?php
include_once APPROOT . '/views/partials/footer.php';
?>