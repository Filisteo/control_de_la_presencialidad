<?php
include_once APPROOT . '/views/partials/header.php';
include_once APPROOT . '/views/partials/navbar.php';
?>
    
    <div id="login-div">
        <img id="img-login" src="<?= URLROOT ?>/public/img/pngegg.png" alt="">
        <h1 class="text-primary mx-4 mb-4 text-center">Login</h1>
        <form method="POST" class="mx-4">
            <div class="form-group mb-3 has-validation">
                <label for="email" class="form-label">Email address</label>
                <input type="email" name="email" class="form-control <?= !empty($data['email_err'])? 'is-invalid' : ''?>" value="<?= empty($data['email_err'])? $data['email'] : ''?>">
                <div id="emailHelp" class="form-text invalid-feedback"><?= $data['email_err'] ?></div>
            </div>
            <div class="form-group mb-3 has-validation">
                <label for="password" class="form-label">Password</label>
                <input type="password" name="password" class="form-control <?= !empty($data['password_err'])? 'is-invalid' : ''?>" value="<?= empty($data['password_err'])? $data['password'] : ''?>">
                <div class="form-text invalid-feedback"><?= $data['password_err'] ?></div>
            </div>
            <button id="submit-login" type="submit" class="btn btn-primary">Login</button>
        </form>
    </div>
<?php
include_once APPROOT . '/views/partials/footer.php';
?>