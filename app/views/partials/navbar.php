<nav class="navbar navbar-dark bg-primary p-3">
  <div class="container-fluid">
    <a class="navbar-brand" href="<?= URLROOT?>/paginas/index">
      <img id="icon_nav" src="<?= URLROOT ?>/public/img/icon_nav.png" alt="document with a pen" >
      Presencialidad
    </a>
    <?php if(!isset($_SESSION['name'])){?>
      <a id="a-nav-1" href="<?= URLROOT?>/users/login" class="text-light"><i class="fas fa-sign-in-alt"></i>Login</a>
    <?php }else{ ?>
      <a id="a-nav-1" href="<?= URLROOT?>/users/logout" class="text-light"><i class="fas fa-sign-out-alt"></i>Logout</a>
    <?php } ?>
</nav>