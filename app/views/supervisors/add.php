<?php
include_once APPROOT . '/views/partials/header.php';
include_once APPROOT . '/views/partials/navbar.php';
?>

<div id="login-div">
    <img id="img-login" class="mt-4" src="<?= URLROOT ?>/public/img/PinClipart.com_add-clipart-to-photo_78730.png" alt="add-User">
    <h1 class="text-primary mx-4 mb-4 text-center">Adding a user</h1>
    <form method="POST" class="mx-4 text-primary" enctype="multipart/form-data">

    <div class="form-group mt-3">
            <label for="DNI">DNI: </label>
            <input type="text" name="DNI" class="form-control <?= !empty($data['DNI_err']) ? 'is-invalid' : '' ?>" placeholder="DNI" value="">
            <span class="invalid-feedback"><?= $data['DNI_err'] ?></span>
        </div>

        <div class="form-group mt-3">
            <label for="name">Name: </label>
            <input type="text" name="name" class="form-control <?= !empty($data['name_err']) ? 'is-invalid' : '' ?>" placeholder="Name of the worker" value="">
            <span class="invalid-feedback"><?= $data['name_err'] ?></span>
        </div>

        <div class="form-group">
            <label for="image">Imagen: </label>
            <input type="file" name="image" class="form-control <?= !empty($data['image_err'])? 'is-invalid' : ''?>">
            <span class="invalid-feedback"><?=$data['image_err']?></span>
        </div>

        <div class="form-group mt-3">
            <label for="last-name-1">The first surname: </label>
            <input type="text" name="last-name-1" class="form-control <?= !empty($data['lastname1_err']) ? 'is-invalid' : '' ?>" placeholder="The first surname" value="">
            <span class="invalid-feedback"><?= $data['lastname1_err'] ?></span>
        </div>

        <div class="form-group mt-3">
            <label for="last-name-2">The second surname: </label>
            <input type="text" name="last-name-2" class="form-control <?= !empty($data['lastname2_err']) ? 'is-invalid' : '' ?>" placeholder="The second surname" value="">
            <span class="invalid-feedback"><?= $data['lastname2_err'] ?></span>
        </div>

        <div class="form-group mt-3">
            <label for="phone-number">Phone number: </label>
            <input type="text" name="phone-number" class="form-control <?= !empty($data['phone-number_err']) ? 'is-invalid' : '' ?>" placeholder="Phone number" value="">
            <span class="invalid-feedback"><?= $data['phone-number_err'] ?></span>
        </div>

        <div class="form-group mt-3">
            <label for="email">Email: </label>
            <input type="email" name="email" class="form-control <?= !empty($data['email_err']) ? 'is-invalid' : '' ?>" placeholder="Email@email.com" value="">
            <span class="invalid-feedback"><?= $data['email_err'] ?></span>
        </div>

        <div class="form-group mt-3 mb-3 has-validation">
            <label for="password" class="form-label">Password</label>
            <input type="password" name="password" class="form-control <?= !empty($data['password_err']) ? 'is-invalid' : '' ?>" value="">
            <div class="form-text invalid-feedback"><?= $data['password_err'] ?></div>
        </div>

        <button id="submit-add" type="submit" class="btn btn-primary">Create</button>
        <a class="text-danger text-center" href="<?= URLROOT ?>/users/supervisor"> Return </a>
    </form>
</div>

<?php
include_once APPROOT . '/views/partials/footer.php';
?>