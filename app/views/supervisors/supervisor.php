<?php
include_once APPROOT . '/views/partials/header.php';
include_once APPROOT . '/views/partials/navbar.php';
?>

<div class="flashes">
  <?= (string) flash() ?>
</div>

<h1 class="text-center mt-4 text-primary">Welcome <?= $_SESSION['name'] . '!' ?></h1>
<h2 class="text-center mt-3 text-primary"><?= $data['work']->Nombre_Area ?> supervisor</h2>

<form method="post" class="text-center text-primary mb-4 mt-4">
  <a href="<?= URLROOT ?>/supervisors/add" class="btn btn-primary text-ligth rounded-circle"><i id="insert-user" class="fas fa-user-plus"></i></a>
</form>

<table class="m-4 table table-bordered border-primary table-primary">
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Last name 1</th>
      <th scope="col">Last name 2</th>
      <th scope="col">Email</th>
      <th scope="col">DNI</th>
      <th scope="col">Phone Number</th>
      <th scope="col">Presence</th>
      <th scope="col">Delete</th>
      <th scope="col">Update</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $presence = $data['presence'];

    foreach ($presence as $row) {
    ?>
      <tr>
        <td><?= $row->Nombre; ?></td>
        <td><?= $row->Apellido_1; ?></td>
        <td><?= $row->Apellido_2; ?></td>
        <td><?= $row->Email; ?></td>
        <td><?= $row->DNI; ?></td>
        <td><?= $row->Teléfono; ?></td>
        <!-- Comprueba si el usuario ha asistido al día laboral, comparando la id del Trabajador con su foranea en el turno -->
        <td><?php if ($row->Trabajadores_id != $row->id) { ?>
            <div id="no_presence" class="rounded-circle text-light bg-danger">
              <i id="x" class="fas fa-times"></i>
            </div>
          <?php } else { ?>
            <div id="presence" class="rounded-circle text-light bg-success">
              <i id="bien" class="fas fa-check"></i>
            </div>
          <?php } ?>
        </td>
        <td>
          <form method="POST" action="<?= URLROOT ?>/supervisors/delete/<?= $row->id ?>">
            <button type="submit" class="btn btn-danger btn-block">
              <i class="fas fa-trash"></i> Delete User
            </button>
          </form>
        </td>
        <td>
          <a type="submit" class="btn btn-warning btn-block" href="<?= URLROOT ?>/supervisors/edit/<?= $row->id ?>">
            <i class="fas fa-user-edit"></i> Edit User
          </a>
        </td>
      </tr>
    <?php } ?>
  </tbody>
</table>
<?php
include_once APPROOT . '/views/partials/footer.php';
?>