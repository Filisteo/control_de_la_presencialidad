<?php 
if( !session_id() ) @session_start();
use \Dotenv\Dotenv;
// Initialize Composer Autoload
require_once 'vendor/autoload.php';
$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();
require_once 'exceptions/FileException.php';
require_once 'config/config.php';
require_once 'helpers/urlHelpper.php';
require_once 'helpers/number.php';
require_once 'helpers/isLoged.php';

spl_autoload_register(function ($className) {
    require_once 'libraries/'.$className.'.php';
});

require_once 'controllers/Paginas.php';
require_once 'controllers/Users.php';

?>