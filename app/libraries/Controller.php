<?php

class Controller {

    public function __construct()
    {
        
    }

    public function model ($modelo) {

        require_once '../app/models/'.$modelo.'.php';

        $model = new $modelo();

        return $model;

    }

    public function view ($vista, $data=[]) {
       if(file_exists('../app/views/' . $vista . '.php')){
            require_once '../app/views/' . $vista . '.php';   
        }else{
            echo "No existe la vista";
        }

    }

}

?>