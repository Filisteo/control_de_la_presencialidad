<?php

class FileException extends Exception
{
    public function __construct(string $message)
    {
        //Sirve para acceder a constantes o métodos estáticos de la clase padre
        parent::__construct($message);
    }
}

?>